function About() {
  return (
    <main className="About">
      <h2>About</h2>
      <p style={{ marginTop: "1rem" }}>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita
        aspernatur tempora, modi sequi facere eius dolore debitis provident sit
        fuga minus id dolorem quam, autem natus nostrum molestiae! Cupiditate,
        rem.
      </p>
    </main>
  );
}

export default About;
